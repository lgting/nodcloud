<?php
	error_reporting(0);
	header('Content-Type: text/html; charset=utf-8');
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>点可云进销存管理系统安装</title>
		<link rel="stylesheet" type="text/css" href="css/metinfo.css" />
		<link rel="stylesheet" type="text/css" href="css/reset.css" />
	</head>
	<body>
		<div id="jsheit">
			<div class="contenttext round" style="text-align: center;">
				<h1 class="titleok">恭喜您，安装成功！</h1>
				<p>从今天开始，点可云进销存管理系统将为您服务，感谢您的信任与支持！</p>
				<p style="color:#666;">建议您删除/install/目录，以防止再次安装而覆盖数据。</p>
                <p>1.本项目（V6）版本可以用于商业用途，但仅限于保留版权的形式自用。</p>
                <p>2.如对本项目（V6）版本二次销售的（：所有以本项目（V6）进行任何盈利性行为的），请务必联系官方授权。</p>
                <p>3.（重要）未经授权禁止将本项目（V6）的代码和资源进行任何收费形式的传播，不得以本项目（V6）进行任何盈利性行为。否则我们将第一时间进行维权。</p>

                <p>点可云公司，助力开源事业，打造纯粹的开源生态。</p>
                <p>严厉打击破坏点可云开源生态的一切行为。</p>

                <p>说明：</p>
                <p>1.点可云v6版本不提供任何的技术支持和售后服务，如必要可联系我们付费处理，或您可以在社区内寻找解决方法。</p>
                <p>2.如您商业使用，建议使用V7 V8商业版，可享受技术支持，售后服务，更新服务。</p>
				<div style="margin-top:5px;">
					<a class="getweburl" href="/?nodcloud" target="_parent">点此登陆系统</a>
				</div>
			</div>
		</div>
	</body>
</html>